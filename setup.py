#!/usr/bin/env python

import sys

sys.path.insert(0, 'setuptools-0.6c6-py2.4.egg')
from setuptools import setup, find_packages
from pysize.version import VERSION

setup(name='pysize',
      version=VERSION,
      description='Directory space analyzer',
      long_description=
'''Pysize graphically represents the content of directories as boxes
proportional to the relative size of their content. It can be used either
in console mode where it generates some ASCII art or with its GTK+ GUI.
Its caching property makes it suitable to browse very large directories.''',
      author='Guillaume Chazarain',
      author_email='guichaz@gmail.com',
      url='http://guichaz.free.fr/pysize',
      scripts=['bin/pysize'],
      packages=find_packages(),
      include_package_data=True,
      license='GPL'
)
