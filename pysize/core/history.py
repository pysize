# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

from pysize.core.observable import observable

# [(fullpaths, name)]
history = []
next_insertion_index = 0
history_observable = observable()

def add_entry(tree):
    global next_insertion_index
    fp = tree.fullpaths
    if not fp:
        return
    same = next_insertion_index and fp == history[next_insertion_index - 1][0]
    if not same:
        if next_insertion_index == len(history):
            # History is being written
            history.append(None)
        else:
            paths, name = history[next_insertion_index]
            if fp != paths:
                # History took another path, clear the remaining forward history
                del history[next_insertion_index + 1:]
        history[next_insertion_index] = fp, tree.root.get_name()
        next_insertion_index += 1
        history_observable.fire_observers(next_insertion_index, history)

def go_to_history(index):
    global next_insertion_index
    entry = history[index][0]
    next_insertion_index = index + 1
    history_observable.fire_observers(next_insertion_index, history)
    return entry

def forward():
    if len(history) > next_insertion_index:
        return go_to_history(next_insertion_index)

def back():
    if next_insertion_index > 1:
        return go_to_history(next_insertion_index - 2)
