# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import stat

from pysize.core.browsing import browsedir
from pysize.core.observable import observable
from pysize.core.pysize_global_fs_cache import get_dev_ino, cache_add_dir
from pysize.core.pysize_global_fs_cache import cache_add_hardlink
from pysize.core.pysize_global_fs_cache import cache_get_dir_size
from pysize.core.deletion import get_deletion_info, DELETED
from pysize.core.exception_propagation import catches

size_observable = observable()

def log_error(text, e):
    print text, e

@catches(OSError)
def _fast(fullpath, cross_device, error_cb):
    """Return the size of the file or directory at path, using or updating the
    cache.
    """
    assert fullpath[0] == '/', fullpath
    try:
        size_observable.fire_observers()
        st = os.lstat(fullpath)
        if stat.S_ISDIR(st.st_mode): # Directory
            dev_ino = st.st_dev, st.st_ino
            cached_size = cache_get_dir_size(dev_ino)
            if cached_size is not None:
                return cached_size
            size = st.st_blocks * 512
            for child in browsedir(fullpath, cross_device,
                                    account_deletion=False):
                size += _fast(child, cross_device, error_cb)
            cache_add_dir(dev_ino, size)
            return size
        if st.st_nlink > 1: # Hardlink
            dir_ino = get_dev_ino(os.path.dirname(fullpath))[1]
            if cache_add_hardlink(st.st_dev, st.st_ino, dir_ino, fullpath):
                return st.st_blocks * 512
            return 0
        # Otherwise, it's a regular file
        return st.st_blocks * 512
    except OSError, e:
        error_cb('size(%s)' % (fullpath), e)
        return 0

def slow(fullpath, cross_device=True, error_cb=log_error,
         account_deletion=True):
    """Same as _size_fast(path, ...), except that deletion can be
    accounted."""
    size = _fast(fullpath, cross_device, error_cb)
    if account_deletion:
        deletion_info = get_deletion_info(fullpath)
        if deletion_info is DELETED:
            size = 0
        else:
            size -= deletion_info
    return size

def slow_sum(fullpaths, cross_device=True, error_cb=log_error,
             account_deletion=True):
    res = 0
    for p in fullpaths:
        res += slow(p, cross_device, error_cb, account_deletion)
    return res
