# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import signal
import sys
import traceback

def install_sigquit_handler():
    def sigquit_handler(signum, frame):
        print 'SIGQUIT triggered traceback (most recent call last):'
        try:
            # Python-2.5
            frames = sys._current_frames().values()
        except AttributeError:
            # Python-2.4
            frames = [frame]
        for f in frames:
            print '======================'
            traceback.print_stack(f)
    signal.signal(signal.SIGQUIT, sigquit_handler)

def install_sigint_handler():
    def sigint_handler(signum, frame):
        os.kill(0, signal.SIGTERM)
    signal.signal(signal.SIGINT, sigint_handler)

