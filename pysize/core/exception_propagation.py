import sys
import threading
import traceback

# The checks can be too expensive, so they can be disabled
ENABLE_CHECKS = True

local = threading.local()
RAISES, CATCHES, UNEXPECT = range(3)

# To be overriden by the test
def _log_error(msg):
    print >> sys.stderr, msg

def _get_exception_stack():
    """Get our thread local exception types stack"""
    try:
        stack = local.stack
    except AttributeError:
        stack = local.stack = []
    return stack

def _find_subclasses(parents, children):
    subclasses = []
    for c in children:
        if issubclass(c, parents):
            subclasses.append(c)
    return subclasses

# Check the top of the stack against the previous elements
# By induction, the whole stack is always consistent
def _check_stack(stack):
    top_action, top_types = stack[-1]
    top_types = list(top_types) # We are going to change it
    if top_action == RAISES:
        for i in xrange(len(stack) - 2, -1, -1):
            action, types = stack[i]
            intersection = _find_subclasses(types, top_types)
            if intersection:
                if action == UNEXPECT:
                    _log_error('Possible unexpected exceptions: ' +
                              str(intersection) + ' at:\n' +
                              ''.join(traceback.format_stack()))
                for c in intersection:
                    top_types.remove(c)

def _decorate_function(function, types, action, msg_prefix):
    if not ENABLE_CHECKS:
        return function
    def new_function(*args, **kwargs):
        assert types
        stack = _get_exception_stack()
        item = (action, types)
        stack.append(item)
        try:
            _check_stack(stack)
            try:
                return function(*args, **kwargs)
            except:
                if msg_prefix:
                    exc_type, exc, trace = sys.exc_info()
                    if issubclass(exc_type, types):
                        _log_error(msg_prefix + ' exception: ' + str(exc_type) +
                                  ': ' + str(exc) + ' at:\n' +
                                  ''.join(traceback.format_tb(trace)))
                raise
        finally:
            top = stack.pop()
            assert top == item
    return new_function

def raises(*types):
    return lambda f: _decorate_function(f, types, RAISES, None)

def catches(*types):
    return lambda f: _decorate_function(f, types, CATCHES, 'Uncaught')

def unexpect(*types):
    return lambda f: _decorate_function(f, types, UNEXPECT, 'Unexpected')

## Tests

if __name__ == '__main__':
    _checkpoints = 0
    def checkpoint(nr):
        global _checkpoints
        _checkpoints += 1
        assert _checkpoints == nr

    errors = ''
    _log_error('Testing')
    def _log_error(msg):
        global errors
        errors += msg

    class MyException(Exception):
        pass

    @raises(MyException)
    def advertiseRaise():
        pass

    def unadvertisedRaise():
        raise MyException()

    @unexpect(MyException)
    def unexpectedRaise():
        unadvertisedRaise()

    @catches(MyException)
    def caughtRaise():
        advertiseRaise()
        try:
            unadvertisedRaise()
        except MyException:
            checkpoint(4)

    @unexpect(MyException)
    def nestedUnexpectedRaise():
        unexpectedRaise()

    @catches(MyException)
    def uncaughtException():
        unadvertisedRaise()

    @unexpect(MyException)
    def test():
        advertiseRaise()

        checkpoint(1)
        try:
            unadvertisedRaise()
        except MyException:
            checkpoint(2)
        try:
            unexpectedRaise()
        except MyException:
            checkpoint(3)
        caughtRaise()
        try:
            nestedUnexpectedRaise()
        except MyException:
            checkpoint(5)

        try:
            uncaughtException()
        except MyException:
            checkpoint(6)

        lines = [err for err in errors.split('\n') if not err.startswith(' ')]
        assert lines == [
         "Possible unexpected exceptions: [<class '__main__.MyException'>] at:",
         "Unexpected exception: <class '__main__.MyException'>:  at:",
         "Unexpected exception: <class '__main__.MyException'>:  at:",
         "Unexpected exception: <class '__main__.MyException'>:  at:",
         "Uncaught exception: <class '__main__.MyException'>:  at:", '']
        checkpoint(7)
    test()

