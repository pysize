# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
from pysize.core.exception_propagation import raises

# The cache for directories sizes
# {device: {directory_inode: size}}
DEV_DIR_SIZE = {}

# The cache for hardlinks locations
# {device: {hardlink_inode: (directory_inode, basename)}}
# For memory efficiency reasons, we suppose directory_inode lives
# in the same device as hardlink_inode, but this can be defeated with
# mount --bind on files. So if we have the same hardlink device/inode
# in two directories with the same inode on different devices, we may
# count both instances as canonical, and then counting many times the
# hardlink size. But this is very unlikely, unless on purpose, but then
# you get what you deserve :-p
DEV_HLINK_DNAME = {}

@raises(OSError)
def get_dev_ino(path):
    """Return (device_number, inode) for path."""
    st = os.lstat(path)
    return st.st_dev, st.st_ino

def cache_add_dir((dev, ino), size):
    """Add a directory and its size to the cache."""
    inodes = DEV_DIR_SIZE.setdefault(dev, {})
    inodes[ino] = size

def cache_add_hardlink(dev, ino, dir_ino, path):
    """Add a hardlink and its location to the cache, and returns True iff the
    location should be considered as the canonical location. Unlike for the
    canonical location, a hardlink size is 0."""
    entry = dir_ino, os.path.basename(path)
    inodes = DEV_HLINK_DNAME.setdefault(dev, {})
    cached_entry = inodes.setdefault(ino, entry)
    return cached_entry == entry

def cache_get_dir_size((dev, ino)):
    """Retrieve a directory size from the cache."""
    inodes = DEV_DIR_SIZE.get(dev, None)
    if inodes:
        return inodes.get(ino, None)

def drop_caches():
    DEV_DIR_SIZE.clear()
    DEV_HLINK_DNAME.clear()
