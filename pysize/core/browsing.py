# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import stat

from pysize.core.deletion import filter_deleted

def browsedir(fullpath, cross_device=True, account_deletion=True):
    try:
        basenames = os.listdir(fullpath)
    except OSError, e:
        print e
        return []
    if account_deletion:
        basenames = filter_deleted(fullpath, basenames)
    if not cross_device:
        def same_dev(basename):
            test_fullpath = fullpath + '/' + basename
            try:
                st = os.lstat(test_fullpath)
            except OSError:
                return False
            if st.st_dev != dev:
                return False
            if not stat.S_ISDIR(st.st_mode):
                return True
            # Some automount-like directories (~/.snapshot) reveal their
            # true st_dev after being opened.
            try:
                fd = os.open(test_fullpath, os.O_RDONLY)
            except OSError:
                return False
            try:
                st = os.fstat(fd)
            except OSError:
                st = None
            try:
                os.close(fd)
            except OSError:
                return False
            return st and st.st_dev == dev

        try:
            dev = os.lstat(fullpath).st_dev
        except OSError, e:
            print e
            dev = None
        basenames = filter(same_dev, basenames)
    # We want the order to be always the same so that hard links
    # detection will be consistent across filesystems
    basenames.sort()
    fullpath = fullpath.rstrip('/')
    return [fullpath + '/' + basename for basename in basenames]
