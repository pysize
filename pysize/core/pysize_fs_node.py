# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import stat

from pysize.core.browsing import browsedir
from pysize.core import compute_size
from pysize.core.pysize_global_fs_cache import cache_add_dir
from pysize.core.deletion import get_deletion_info, DELETED

def _extract_prefix_suffixes(fullpaths):
    """['/prefix/suffix1', '/prefix/suffix2', '/prefix/suffix3'] =>
        ('/prefix', ['suffix1', 'suffix2', 'suffix3'])"""
    prefix_len = os.path.commonprefix(fullpaths).rfind('/') + 1
    prefix = fullpaths[0][:prefix_len - 1] or '/'
    suffixes = [p[prefix_len:] for p in fullpaths]
    return prefix, suffixes

def _join_prefix_suffixes(prefix, suffixes):
    if len(suffixes) == 1:
        suffix = suffixes[0]
    else:
        suffix = '{' + ','.join(suffixes) + '}'
    return os.path.join(prefix, suffix)

def pretty_paths(fullpaths):
    prefix, suffixes = _extract_prefix_suffixes(fullpaths)
    return _join_prefix_suffixes(prefix, suffixes)

class _pysize_node(object):
    """The parent class of all displayed nodes, as these nodes are displayed on
    screen, there should be few instances."""
    def __init__(self, parent, basename, options):
        self.rectangle = None
        self.parent = parent
        self.children = []
        self.basename = basename
        if basename:
            self.size = compute_size.slow(self.get_fullname(),
                                          options.cross_device)
        else:
            self.size = 0

    def compute_height(self):
        if self.children:
            children_height = max([c.compute_height() for c in self.children])
            return children_height + 1
        return 0

    def compute_depth(self):
        depth = 0
        node = self
        while node:
            node = node.parent
            depth += 1
        return depth

    def minimum_node_size(self):
        res = self.size
        for child in self.children:
            child_min_node_size = child.minimum_node_size()
            res = min(res, child_min_node_size)
        return res

    def get_dirname(self):
        return os.path.dirname(self.get_fullpaths()[0])

    def is_dir(self):
        return False

    def is_real(self):
        """Does the node correspond to a path that actually exists?"""
        return True

    def get_fullname(self):
        return pretty_paths(self.get_fullpaths())

    def get_fullpaths(self):
        parent = self.parent
        fullpath = self.basename
        while parent:
            fullpath = os.path.join(parent.basename, fullpath)
            parent = parent.parent
        return [fullpath]

    def get_name(self):
        """Return the name that should be displayed for this node."""
        return self.basename

    def __iter__(self):
        """The iterator is a depth first traversal."""
        yield self
        for child in self.children:
            for node in child:
                yield node

    def contains_point(self, p):
        """Is the point p in the graphical representation of this node?"""
        if not self.rectangle:
            return False
        x0, x1, y0, y1 = self.rectangle
        return x0 < p.x and p.x < x1 and y0 < p.y and p.y < y1

class _pysize_node_collection(_pysize_node):
    """A directory"""
    def __init__(self, parent, prefix_basename, children_fullpaths, max_depth,
                 min_size, options):
        _pysize_node.__init__(self, parent, None, options)
        self.basename = prefix_basename
        if max_depth == 0:
            self.size = compute_size.slow_sum(children_fullpaths,
                                              options.cross_device)
        else:
            children_size = 0
            remaining_fullpaths = []
            remaining_size = 0
            for child_fullpath in children_fullpaths:
                child_basename = os.path.basename(child_fullpath)
                try:
                    node = create_node(self, [child_basename], [child_fullpath],
                                       max_depth - 1, min_size, options)
                except OSError:
                    print child_fullpath, 'disappeared'
                    continue
                if node.is_real():
                    self.children.append(node)
                    children_size += node.size
                else:
                    remaining_fullpaths.append(child_fullpath)
                    remaining_size += node.size

            self.children.sort(cmp=lambda n1, n2: cmp(n2.size, n1.size) or
                                              cmp(n1.get_name(), n2.get_name()))
            if remaining_size > min_size:
                rem = _pysize_node_remaining(self, remaining_fullpaths, options)
                self.children.append(rem)
            self.size = children_size + remaining_size

    def is_real(self):
        """This node does not actually exists, it is an aggregate."""
        return False

class _pysize_node_forest(_pysize_node_collection):
    def __init__(self, parent, children_fullpaths, max_depth, min_size,
                 options):
        assert parent is None
        prefix, suffixes = _extract_prefix_suffixes(children_fullpaths)
        self.trees = suffixes
        _pysize_node_collection.__init__(self, parent, prefix,
                                         children_fullpaths, max_depth,
                                         min_size, options)

    def get_name(self):
        return _join_prefix_suffixes(self.basename, self.trees)

    def get_fullpaths(self):
        return [self.basename + '/' + t for t in self.trees]

    def get_dirname(self):
        return self.basename

class _pysize_node_dir(_pysize_node_collection):
    """A directory"""
    def __init__(self, parent, basename, st, max_depth, min_size, options):
        # Needed for get_fullname() before _pysize_node_collection.__init__()
        self.parent = parent
        self.basename = basename
        fullpath = self.get_fullname()
        listing = browsedir(fullpath, options.cross_device)
        _pysize_node_collection.__init__(self, parent, basename, listing,
                                         max_depth, min_size, options)
        self.basename = basename
        self.size += st.st_blocks * 512
        # update size in cache, in case files were added/removed
        deletion_info = get_deletion_info(fullpath)
        if deletion_info is not DELETED:
            dev_ino = (st.st_dev, st.st_ino)
            cache_add_dir(dev_ino, self.size + deletion_info)

    def is_dir(self):
        return True

    def is_real(self):
        return True

    def get_name(self):
        name = self.basename
        if name != '/':
            name += '/'
        return name

class _pysize_node_remaining(_pysize_node_collection):
    """The sum of a directory's children that are too small to be drawn."""
    def __init__(self, parent, fullpaths, options):
        _pysize_node.__init__(self, parent, None, options)
        # The parent constructor would visit the files
        self.size = compute_size.slow_sum(fullpaths, options.cross_device)
        size_basename = [(compute_size.slow(fp), os.path.basename(fp)) for
                                                                fp in fullpaths]
        size_basename.sort(key=lambda s_b: s_b[0], reverse=True)
        self.remaining_elements = [b for s, b in size_basename]

    def get_name(self):
        return '{' + ','.join(self.remaining_elements) + '}'

    def get_fullname(self):
        if self.remaining_elements:
            return _pysize_node_collection.get_fullname(self)
        return '' # This is the initial node

    def get_fullpaths(self):
        fullpaths = self.remaining_elements
        parent = self.parent
        while parent:
            fullpaths = [os.path.join(parent.basename, fp) for fp in fullpaths]
            parent = parent.parent
        return fullpaths

class _pysize_node_file(_pysize_node):
    """A file"""
    def __init__(self, parent, basename, options):
        _pysize_node.__init__(self, parent, basename, options)

class _pysize_node_hardlink(_pysize_node_file):
    """A hardlink, the canonical one, or a link"""
    def __init__(self, parent, basename, options):
        _pysize_node_file.__init__(self, parent, basename, options)

def create_node(parent, basenames, fullpaths, max_depth, min_size, options):
    """Return a pysize_node for parent/basename traversing up to max_depth
    levels and only taking into account elements bigger than min_size."""
    if not basenames:
        return _pysize_node_remaining(parent, [], options)

    assert len(basenames) == len(fullpaths)
    size = compute_size.slow_sum(fullpaths, options.cross_device)
    if size < min_size:
        node = _pysize_node_remaining(parent, fullpaths, options)
    elif len(basenames) > 1:
        node = _pysize_node_forest(parent, basenames, max_depth, min_size,
                                   options)
    else:
        try:
            st = os.lstat(fullpaths[0])
        except OSError, e:
            print e
            return _pysize_node_remaining(parent, [], options)
        if stat.S_ISDIR(st.st_mode):
            node = _pysize_node_dir(parent, basenames[0], st, max_depth,
                                    min_size, options)
        elif st.st_nlink > 1:
            node = _pysize_node_hardlink(parent, basenames[0], options)
        else:
            node = _pysize_node_file(parent, basenames[0], options)
    return node
