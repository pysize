# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os

from pysize.core.pysize_global_fs_cache import get_dev_ino
from pysize.core.exception_propagation import catches, unexpect

# ['/path/to/file1', '/path/to/file2']
TO_DELETE = []

# {device: {inode: (size_to_subtract|DELETED)}}
DEV_INO_TO_SUBTRACT = {}

# Deleted inodes are marked as such, instead of subtracting their whole size
# as the latter may change
class deleted(object):
    pass
DELETED = deleted()

@unexpect(OSError)
def _iter_from_the_root(fullpath):
    """If path is /a/b/c, iterates over /, /a, /a/b, /a/b/c and returns
    for each path the inodes dictionary in DEV_INO_TO_SUBTRACT as well as the
    inode number"""
    rebuilt = '/'
    for element in [''] + fullpath.strip('/').split('/'):
        rebuilt = os.path.join(rebuilt, element)
        dev, ino = get_dev_ino(rebuilt)
        yield DEV_INO_TO_SUBTRACT.setdefault(dev, {}), ino

@catches(OSError)
def get_deletion_info(fullpath):
    """Get the size to subtract to the size of the given path, the returned
    value can be:
      o The size to subtract
      o DELETED if the given path has been deleted"""
    subtracted = 0
    if DEV_INO_TO_SUBTRACT:
        try:
            for inodes, ino in _iter_from_the_root(fullpath):
                subtracted = inodes.get(ino, 0)
                if subtracted == 0:
                    # Nothing to delete
                    break
                if subtracted is DELETED:
                    # The prefix is deleted, so the complete path too
                    break
                # Otherwise the prefix contains some deleted items, so we must
                # continue
        except OSError, e:
            print e
            return DELETED
    return subtracted

def _add_to_hash(fullpath):
    """Add the path to the hash table, including its parents"""
    from pysize.core import compute_size
    size = compute_size.slow(fullpath)
    dev, ino = get_dev_ino(fullpath)
    DEV_INO_TO_SUBTRACT.setdefault(dev, {})[ino] = DELETED
    for inodes, ino in _iter_from_the_root(os.path.dirname(fullpath)):
        inodes[ino] = inodes.get(ino, 0) + size

# TODO: Deletion of hard links
@catches(OSError)
def schedule(nodes):
    """Simulate the deletion of some files"""
    for node in nodes:
        for fullpath in node.get_fullpaths():
            to_delete = False
            try:
                subtract = get_deletion_info(fullpath)
                if subtract is not DELETED:
                    _add_to_hash(fullpath)
                    to_delete = True
            except OSError, e:
                print e
            if to_delete:
                TO_DELETE.append(fullpath)

    _prune_list()
    _rebuild_hash()

def _prune_list():
    """Remove children if we have the parent"""
    while True:
        for i in xrange(len(TO_DELETE)):
            parent = os.path.dirname(TO_DELETE[i])
            if get_deletion_info(parent) is DELETED:
                del TO_DELETE[i]
                break
        else:
            break

def _rebuild_hash():
    """Rebuild the hash table from the list of deleted files"""
    DEV_INO_TO_SUBTRACT.clear()
    for fullpath in TO_DELETE:
        _add_to_hash(fullpath)

def filter_deleted(dirname, basenames):
    """Basenames are files in the current directory"""
    if not DEV_INO_TO_SUBTRACT:
        return basenames
    subtracted_prefix = get_deletion_info(dirname)
    if subtracted_prefix is DELETED:
        return []
    if not subtracted_prefix:
        return basenames
    @catches(OSError)
    def is_not_deleted(basename):
        try:
            dev, ino = get_dev_ino(dirname + '/' + basename)
            return DEV_INO_TO_SUBTRACT.get(dev, {}).get(ino, 0) is not DELETED
        except OSError, e:
            print e
            return False
    return filter(is_not_deleted, basenames)

def get_deleted():
    """Return a copy to permit modifications while iterating"""
    return TO_DELETE[:]

@catches(OSError)
def restore(fullpath):
    """Cancel the deletion of the given path"""
    from pysize.core import compute_size
    try:
        dev, ino = get_dev_ino(fullpath)
    except OSError, e:
        print e
        return
    TO_DELETE.remove(fullpath)
    del DEV_INO_TO_SUBTRACT[dev][ino]
    size = compute_size.slow(fullpath, account_deletion=False)
    for inodes, ino in _iter_from_the_root(os.path.dirname(fullpath)):
        if inodes[ino] == size:
            del inodes[ino]
        else:
            inodes[ino] -= size
