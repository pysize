# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import sys
import os
import optparse
import locale

if sys.hexversion < 0x02040000:
    print >> sys.stderr, 'Your python version is too old (%s)' % \
                                                        (sys.version.split()[0])
    print >> sys.stderr, 'You need at least Python 2.4'
    sys.exit(1)

from pysize.ui.ascii import ui_ascii
from pysize.ui.curses import ui_curses
from pysize.ui.gtk import ui_gtk
from pysize.ui.utils import UINotAvailableException
from pysize.version import VERSION
from pysize.core.exception_propagation import unexpect

def _ui_auto(options, args):
    """Automatically choose the best available UI."""
    for ui_run in ui_gtk.run, ui_curses.run, ui_ascii.run:
        try:
            ui_run(options, args)
            return
        except UINotAvailableException:
            pass

    raise UINotAvailableException

UI = {'ascii': ui_ascii.run, 'curses': ui_curses.run,
      'gtk': ui_gtk.run, 'auto': _ui_auto}

def _profile(continuation):
    prof_file = 'pysize.prof'
    try:
        import cProfile
        import pstats
        print 'Profiling using cProfile'
        cProfile.runctx('continuation()', globals(), locals(), prof_file)
        stats = pstats.Stats(prof_file)
    except ImportError:
        import hotshot
        import hotshot.stats
        prof = hotshot.Profile(prof_file)
        print 'Profiling using hotshot'
        prof.runcall(continuation)
        prof.close()
        stats = hotshot.stats.load(prof_file)
    stats.strip_dirs()
    stats.sort_stats('time', 'calls')
    stats.print_stats(40)
    os.remove(prof_file)

def _try_psyco():
    try:
        # Try to use psyco if available
        import psyco
        psyco.full()
    except ImportError:
        pass

def setprocname(name):
    # From comments on http://davyd.livejournal.com/166352.html
    try:
        # For Python-2.5
        import ctypes
        libc = ctypes.CDLL(None)
        # Linux 2.6 PR_SET_NAME
        if libc.prctl(15, name, 0, 0, 0):
            # BSD
            libc.setproctitle(name)
    except:
        try:
            # For 32 bit
            import dl
            libc = dl.open(None)
            name += '\0'
            # Linux 2.6 PR_SET_NAME
            if libc.call('prctl', 15, name, 0, 0, 0):
                # BSD
                libc.call('setproctitle', name)
        except:
            pass

@unexpect(Exception)
def main():
    _try_psyco()
    locale.setlocale(locale.LC_ALL, '')
    setprocname('pysize')
    usage = '%s [OPTIONS] [DIRECTORIES...]' % (sys.argv[0])
    parser = optparse.OptionParser(usage=usage, version='pysize ' + VERSION)
    parser.add_option('--ui', type='choice', choices=UI.keys(), default='auto',
                      help='choose the ui between: [auto], gtk, curses, ascii')
    parser.add_option('--xdev', action='store_false', dest='cross_device',
                      default=True,
                      help='ignore directories on other filesystems')
    parser.add_option('--max-depth', type='int', dest='max_depth', default=6,
                      metavar='DEPTH',
                      help='maximum depth of the displayed tree [6]')
    parser.add_option('--profile', action='store_true', dest='profile',
                      default=False, help=optparse.SUPPRESS_HELP)
    options, args = parser.parse_args()

    if options.max_depth < 2:
        parser.error('maximum depth must be at least 2')

    args = map(os.path.realpath, args)
    def continuation():
        try:
            UI[options.ui](options, args)
        except UINotAvailableException:
            print 'The interface "%s" is not available' % (options.ui)

    if options.profile:
        _profile(continuation)
    else:
        continuation()
