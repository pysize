# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import time

UNITS = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB']

def human_unit(size):
    """Return a string of the form '12.34 MiB' given a size in bytes."""
    for i in xrange(len(UNITS) - 1, 0, -1):
        base = 1 << (10 * i)
        if 2 * base < size:
            return '%.2f %s' % ((float(size) / base), UNITS[i])
    return str(size) + ' ' + UNITS[0]

def sanitize_string(string, max_length=None):
    try:
        decoded = string.decode('UTF-8')
    except UnicodeDecodeError:
        def robust_char_decode(c):
            try:
                return c.decode('UTF-8')
            except UnicodeDecodeError:
                return '?'
        decoded = ''.join(map(robust_char_decode, string))
    if max_length is not None and len(decoded) > max_length:
        nr_dots = max(0, min(3, max_length - 1))
        decoded = decoded[:max_length - nr_dots] + '.' * nr_dots
    for c in decoded:
        if c.isspace() and c != ' ':
            def clean_blank(c):
                if c.isspace() and c != ' ':
                    c = '?'
                return c
            return ''.join(map(clean_blank, decoded))
    return decoded

PROGRESS_CHARS = ['/', '-', '\\', '|']
last_progress_char = 0
last_progress_time = time.time()

def update_progress():
    global last_progress_char, last_progress_time
    now = time.time()
    if now - last_progress_time > 0.04:
        last_progress_char = (last_progress_char + 1) % len(PROGRESS_CHARS)
        last_progress_time = now
        return PROGRESS_CHARS[last_progress_char]

class UINotAvailableException(Exception):
    pass
