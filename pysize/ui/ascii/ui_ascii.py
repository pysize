# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import math
import os
import sys

from pysize.core.pysize_fs_tree import pysize_tree
from pysize.core.compute_size import size_observable
from pysize.ui.utils import human_unit, update_progress
from pysize.ui.utils import sanitize_string
from pysize.ui.char_matrix import CharMatrix, HLINE_START, HLINE, HLINE_END
from pysize.ui.char_matrix import VLINE_START, VLINE, VLINE_END
from pysize.ui.ascii.terminal_size import terminal_size

MATRIX_TO_ASCII = {
    0: ' ',
    HLINE_START: '-', HLINE: '-', HLINE_END: '-',
    VLINE_START: '|', VLINE: '|', VLINE_END: '|'
}

def _transform(char):
    if isinstance(char, int):
        return MATRIX_TO_ASCII.get(char, '+')
    return char

def _draw_progress():
    progress = update_progress()
    if progress:
        print '', progress, '\r',
        sys.stdout.flush()

def run(options, args):
    columns, lines = terminal_size()
    lines -= 3 # Summary + Prompt + Last line
    size_observable.add_observer(_draw_progress)
    args = args or [os.getcwd()]
    # An entry needs 2 lines
    tree = pysize_tree(args, options.max_depth, 2.0 / lines, options)
    if not tree.root:
        sys.exit(1)
    # The last entry needs an additional terminating line
    total_lines = int(math.ceil(2.0 * tree.root.size /
                                max(1, tree.root.minimum_node_size()))) + 1
    matrix = CharMatrix(columns, total_lines, tree)
    print '\n'.join([''.join(map(_transform, line)) for line in matrix.matrix])
    print sanitize_string(tree.root.get_name()), human_unit(tree.root.size)
