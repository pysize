# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import pygtk
pygtk.require('2.0')
import gtk
assert gtk.pygtk_version >= (2, 8)

class _point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

def _event_point(event):
    if event.is_hint:
        x, y, state = event.window.get_pointer()
        point = _point(x, y)
    else:
        point = _point(event.x, event.y)
    return point

class PysizeWidget_Mouse(object):
    def __init__(self, options, args):
        self.connect('motion-notify-event', type(self)._motion_notify_event)
        self.connect('button-press-event', type(self)._button_press_event)
        self.connect('button-release-event', type(self)._button_release_event)
        self.connect('building-tree-state-changed', type(self)._reset_mouse)
        self.cursor_node = None
        self.button_press_node = None
        self.selected_nodes = set()
        self.set_cursor_node(None)

    def set_cursor_node(self, cursor_node):
        self.cursor_node = cursor_node
        self.emit('hover-changed', cursor_node)

    def _reset_mouse(self, unused_tree):
        self.set_cursor_node(None)
        self.button_press_node = None

    def _get_node_here(self, event):
        point = _point(event.x, event.y)
        for node in self.tree.root:
            if node.contains_point(point):
                return node

    def _motion_notify_event(self, event):
        point = _event_point(event)
        prev_selection = new_selection = self.cursor_node
        if not (self.cursor_node and self.cursor_node.contains_point(point)):
            # The cursor is no more in the same node
            mask = 0x00
            for node in self.tree.root:
                if node.contains_point(point) != (node == prev_selection):
                    if node == prev_selection:
                        new_selection = None
                        mask |= 0x1
                    else:
                        new_selection = node
                        mask |= 0x2
                    if mask == 0x3:
                        break
        if prev_selection != new_selection:
            self.set_cursor_node(new_selection)
            self.queue_node_redraw(prev_selection)
            self.queue_node_redraw(new_selection)
        return True

    def handle_double_click(self, node):
        if node:
            self.set_paths(node.get_fullpaths())
        return node is not None

    def _button_press_event(self, event):
        if event.button == 1:
            node = self._get_node_here(event)
            if event.type == gtk.gdk._2BUTTON_PRESS:
                return self.handle_double_click(node)
            if node != self.button_press_node:
                self.queue_node_redraw(self.button_press_node)
                self.queue_node_redraw(node)
                self.button_press_node = node
            return True
        return False

    def _button_release_event(self, event):
        if event.button == 1:
            node = self._get_node_here(event)
            if node == self.button_press_node:
                if event.state & gtk.gdk.CONTROL_MASK:
                    if node:
                        if node in self.selected_nodes:
                            self.selected_nodes.remove(node)
                        else:
                            self.selected_nodes.add(node)
                elif event.state & gtk.gdk.SHIFT_MASK:
                    if node:
                        is_in_new_selection = False
                        for current in self.tree.breadth_first():
                            if current is node:
                                if is_in_new_selection:
                                    break
                                is_in_new_selection = True
                            if current in self.selected_nodes:
                                if is_in_new_selection:
                                    break
                                is_in_new_selection = True
                            if is_in_new_selection:
                                self.selected_nodes.add(current)
                                self.queue_node_redraw(current)
                        self.selected_nodes.add(node)
                        self.queue_node_redraw(node)
                else:
                    for n in self.get_selected_nodes():
                        self.queue_node_redraw(n)
                    if node:
                        self.selected_nodes = set([node])
                    else:
                        self.selected_nodes = set()
                self.queue_node_redraw(node)
            else:
                self.queue_node_redraw(self.button_press_node)
            self.button_press_node = None
            self.emit('hover-changed', self.cursor_node)
        return True

    def get_selected_nodes(self):
        if self.cursor_node:
            nodes = set([self.cursor_node])
        else:
            nodes = set()
        nodes |= self.selected_nodes
        return nodes
