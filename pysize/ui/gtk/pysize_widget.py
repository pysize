# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import pygtk
pygtk.require('2.0')
import gtk
assert gtk.pygtk_version >= (2, 8)
import gobject

from pysize.core.pysize_global_fs_cache import drop_caches
from pysize.core.pysize_fs_tree import pysize_tree
from pysize.core import history
from pysize.core import deletion
from pysize.ui.gtk.threaded_pysize_tree import threaded_pysize_tree

from pysize.ui.gtk.pysize_widget_draw import PysizeWidget_Draw
from pysize.ui.gtk.pysize_widget_menu import PysizeWidget_Menu
from pysize.ui.gtk.pysize_widget_mouse import PysizeWidget_Mouse


class PysizeWidget(gtk.DrawingArea, PysizeWidget_Draw,
                   PysizeWidget_Menu, PysizeWidget_Mouse):
    __gsignals__ = {
        'hover-changed':
            (gobject.SIGNAL_RUN_LAST,
                gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        'building-tree-state-changed':
            (gobject.SIGNAL_RUN_LAST,
                gobject.TYPE_NONE, (gobject.TYPE_BOOLEAN,))
        }

    def __init__(self, options, args):
        gtk.DrawingArea.__init__(self)
        PysizeWidget_Draw.__init__(self, options, args)
        PysizeWidget_Menu.__init__(self, options, args)
        PysizeWidget_Mouse.__init__(self, options, args)
        self.connect('realize', type(self)._realize)
        self.connect('key-press-event', type(self)._key_press)
        self.connect('configure-event', type(self)._configure)
        self.set_flags(gtk.CAN_FOCUS)
        self.options = options
        self.min_size = 'auto'
        self.paths = args
        self.tree = pysize_tree([], 0, 0, self.options)
        self.tree_builder = threaded_pysize_tree()
        self.tree_builder.completion.add_observer(self._new_tree_callback)

    def _realize(self):
        events = self.window.get_events()
        events |= gtk.gdk.POINTER_MOTION_MASK
        events |= gtk.gdk.POINTER_MOTION_HINT_MASK
        events |= gtk.gdk.KEY_PRESS_MASK
        events |= gtk.gdk.BUTTON_PRESS_MASK
        events |= gtk.gdk.BUTTON_RELEASE_MASK
        self.window.set_events(events)

    def _key_press(self, event):
        directions = {
            gtk.keysyms.Left: self.tree.get_parent,
            gtk.keysyms.Up: self.tree.get_previous_sibling,
            gtk.keysyms.Right: self.tree.get_first_child,
            gtk.keysyms.Down: self.tree.get_next_sibling
        }

        action = directions.get(event.keyval, None)
        if action and self.cursor_node is not None:
            new_selection = action(self.cursor_node)
            if new_selection not in (None, self.tree.root):
                self.queue_node_redraw(self.cursor_node)
                self.queue_node_redraw(new_selection)
                self.set_cursor_node(new_selection)
        return action is not None

    # Called when the threaded_pysize_tree built a new tree for us
    def _new_tree_callback(self, tree):
        if tree:
            self.tree = tree
            self.queue_draw()
            self.invalidate_tree()
            history.add_entry(tree)
        self.emit('building-tree-state-changed', not tree)
        if tree:
            if self.min_size == 'auto':
                height = self.get_parent().allocation.height - 2
            else:
                height = int(self.get_requested_height())
            self.set_size_request(-1, height)

    def complete_reload(self):
        drop_caches()
        self.schedule_new_tree()

    def min_size_requested(self):
        if self.min_size == 'auto':
            return 1.0 / self.max_number_of_nodes()
        return self.min_size

    def set_paths(self, paths):
        if paths:
            self.paths = paths
            self.min_size = 'auto'
            self.schedule_new_tree()

    def schedule_new_tree(self):
        self.tree_builder.schedule(self.paths, self.options.max_depth,
                                   self.min_size_requested(), self.options)

    def _configure(self, event):
        if self.min_size == 'auto':
            self.schedule_new_tree()
        else:
            for node in self.tree.root:
                node.rectangle = None

    def invalidate_tree(self):
        self.emit('hover-changed', None)
        self.selected_nodes = set()

    def parent_directory(self):
        self.set_paths([self.tree.root.get_dirname()])

    def max_depth_changed(self, spin):
        value = spin.get_value()
        self.options.max_depth = value
        self.schedule_new_tree()

    def open(self):
        action = gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER
        buttons = gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, \
                  gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT
        dialog = gtk.FileChooserDialog(title='Select a directory',
                                       action=action, buttons=buttons)
        dialog.set_select_multiple(True)
        response = dialog.run()
        if response == gtk.RESPONSE_ACCEPT:
            paths = map(os.path.realpath, dialog.get_filenames())
            self.set_paths(paths)
        dialog.destroy()

    def schedule_delete(self, nodes):
        deletion.schedule(nodes)
        self.schedule_new_tree()
