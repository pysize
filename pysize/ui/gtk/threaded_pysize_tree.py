# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

from threading import Thread, currentThread
from Queue import Queue
import gobject

from pysize.core.observable import observable
from pysize.core.pysize_fs_tree import pysize_tree
from pysize.core.compute_size import size_observable

# We don't want the building of a tree to block the GUI. A possible
# solution is to periodically call gtk.main_iteration_do() during the
# building. Unfortunately, this introduces reentrance issues when an
# event trigger the building of another tree.
#
# So we build all trees in a single separate thread. This additional
# thread never touches the GUI. So when the GUI wants an updated tree
# it calls threaded_pysize_tree.schedule(), and when the thread finishes
# the tree it wakes the observer that will update the GUI.
#
# As the thread must not touch the GUI, every gtk manipulation must be made
# through a gobject.idle_add(lambda: ...).

# Yeah, we are using threads
gobject.threads_init()

# The only aim of this exception is to unwind the stack in order
# to abort the current building, and quickly procede to the next
# one.
class AbortThisTree(Exception):
    pass

class threaded_pysize_tree(object):
    def __init__(self):
        self.completion = observable()
        self.queue = Queue(0)
        size_observable.add_observer(self.size_observer)
        self.thread = Thread(target=self.run_thread)
        self.thread.setDaemon(True)
        self.thread.start()

    def schedule(self, *args, **kwargs):
        self.queue.put((args, kwargs))

    # Called for every computed size(), that is, periodically during
    # the building of a tree.
    def size_observer(self, *args, **kwargs):
        if not self.queue.empty() and currentThread() == self.thread:
            # The GUI wants another tree, lets stop this one
            raise AbortThisTree

    def run_thread(self):
        while True:
            params = self.queue.get()
            while not self.queue.empty():
                # Get the last item
                params = self.queue.get_nowait()
            args, kwargs = params
            gobject.idle_add(lambda: self.completion.fire_observers(None))
            try:
                tree = pysize_tree(*args, **kwargs)
            except AbortThisTree:
                continue
            gobject.idle_add(lambda: self.completion.fire_observers(tree))
