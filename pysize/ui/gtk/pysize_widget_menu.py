# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import pygtk
pygtk.require('2.0')
import gtk
assert gtk.pygtk_version >= (2, 8)

from pysize.core import compute_size
from pysize.ui.utils import human_unit

class PysizeWidget_Menu(object):
    def __init__(self, options, args):
        self.connect('popup-menu', type(self)._pop_menu)
        self.connect('button-press-event', type(self)._menu_mouse_button)

    @staticmethod
    def _add_menu_item(menu, name, action):
        item = gtk.MenuItem(name)
        item.connect('activate', action)
        item.show()
        menu.append(item)

    def _pop_menu(self, event=None):
        if self.cursor_node:
            menu = gtk.Menu()

            props = lambda item: self.show_properties(self.get_selected_nodes())
            self._add_menu_item(menu, 'Show properties...', props)
            dele = lambda item: self.schedule_delete(self.get_selected_nodes())
            self._add_menu_item(menu, 'Schedule for deletion', dele)
            browse = lambda item: self.handle_double_click(self.cursor_node)
            self._add_menu_item(menu, 'Browse', browse)

            if event:
                button = event.button
                event_time = event.time
            else:
                button = 0
                event_time = gtk.get_current_event_time()
            menu.attach_to_widget(self, None)
            menu.popup(None, None, None, button, event_time)
            return True

    def _menu_mouse_button(self, event):
        if event.button == 3 and event.type == gtk.gdk.BUTTON_PRESS:
            return self._pop_menu(event)
        return False

    def show_properties(self, nodes):
        fullpaths = []
        for node in nodes:
            fullpaths.extend(node.get_fullpaths())
        fullpaths.sort(key=compute_size.slow, reverse=True)
        total_size = 0
        text = ''
        for path in fullpaths:
            size = compute_size.slow(path)
            total_size += size
            if os.path.isdir(path) and path != '/':
                path += '/'
            text += '%s | %s\n' % (human_unit(size), path)
        text += human_unit(total_size)
        names = ','.join([node.get_name() for node in nodes])
        dialog = gtk.Dialog('Properties for ' + names, None, 0,
                            (gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE))
        label = gtk.Label(text)
        label.set_selectable(True)
        label.show()
        scroll = gtk.ScrolledWindow()
        scroll.add_with_viewport(label)
        scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scroll.show()
        dialog.vbox.add(scroll)
        dialog.set_default_size(400, 500)
        dialog.run()
        dialog.destroy()
