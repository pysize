# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

def normalize(*colors):
    return map(lambda x: x / 255.0, colors)

SMALL_DIRECTORY = normalize(145, 166, 245)
BIG_DIRECTORY = normalize(100, 100, 200)

SMALL_FILE = normalize(255, 128, 128)
BIG_FILE = normalize(204, 0, 0)

SMALL_REMAINING = normalize(128, 255, 255)
BIG_REMAINING = normalize(89, 178, 175)

def map_interpol(small, big, interpol, size):
    return map(lambda i: interpol(small[i], big[i], size), xrange(3))

def get_node_colors(node, is_hovered, is_selected, interpol):
    if node.is_real():
        if node.is_dir():
            base_color = map_interpol(SMALL_DIRECTORY, BIG_DIRECTORY,
                                      interpol, node.size)
        else:
            base_color = map_interpol(SMALL_FILE, BIG_FILE,
                                      interpol, node.size)
    else:
        base_color = map_interpol(SMALL_REMAINING, BIG_REMAINING,
                                  interpol, node.size)

    if is_selected:
        base_color = map(lambda x: x * 0.6, base_color)

    if is_hovered:
        base_color = map(lambda x: (x + 1.0) / 2.0, base_color)


    col1 = map(lambda x: min(1.0, x + 0.08), base_color)
    col2 = map(lambda x: max(0.0, x - 0.08), base_color)

    return col1, col2

