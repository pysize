# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import pygtk
pygtk.require('2.0')
import gtk
assert gtk.pygtk_version >= (2, 8)
import pango
import math
import cairo

from pysize.ui.utils import human_unit, sanitize_string
from pysize.ui.gtk.colors import get_node_colors

RADIUS = 10
LINE_WIDTH = 4

class PysizeWidget_Draw(object):
    def __init__(self, options, args):
        self.connect('expose-event', type(self)._expose_event)
        self.modify_font(pango.FontDescription('Monospace 12'))
        self.max_text_height = self.measure_font_height()

    def measure_font_height(self):
        w, h = self.create_pango_layout('a').get_pixel_size()
        return h

    def get_requested_height(self):
        return self.max_text_height * self.tree.root.size / self.min_size

    def queue_node_redraw(self, node):
        if node and node.rectangle:
            x0, x1, y0, y1 = map(int, node.rectangle)
            self.queue_draw_area(x0 - LINE_WIDTH, y0 - LINE_WIDTH,
                                 x1 - x0 + 2*LINE_WIDTH, y1 - y0 + 2*LINE_WIDTH)

    def _make_draw_labels_lambda(self, context, text, (x0, x1, y0, y1),
                                 accept_ellipse=True):
        pl = self.create_pango_layout(text)
        pl.set_alignment(pango.ALIGN_CENTER)
        w = x1 - x0
        h = y1 - y0
        pl.set_width(int(w*pango.SCALE))
        if accept_ellipse:
            ellipse_mode = pango.ELLIPSIZE_END
        else:
            ellipse_mode = pango.ELLIPSIZE_NONE
        pl.set_ellipsize(ellipse_mode)
        real_w, real_h = pl.get_pixel_size()
        line_count = pl.get_line_count()
        line_height = float(real_h) / line_count
        if line_height > self.max_text_height:
            self.max_text_height = line_height
        if line_count == text.count('\n') + 1 and real_w <= w and real_h <= h:
            y0 += (h - real_h) / 2.0
            def draw(context):
                context.move_to(x0, y0)
                context.show_layout(pl)
            return draw

    def _draw_box(self, context, x0, x1, y0, y1, node, interpol):
        is_selected = node in self.selected_nodes
        colors = get_node_colors(node, node == self.cursor_node, is_selected,
                                 interpol)
        context.set_source_rgb(0, 0, 0)
        first_time = not node.rectangle
        context.new_path()
        if first_time:
            if x0 == 0.0:
                x0 += LINE_WIDTH/2.0
            else:
                x0 += LINE_WIDTH/4.0
            x1 -= LINE_WIDTH/4.0
            if y0 == 0.0:
                y0 += LINE_WIDTH/2.0
            else:
                y0 += LINE_WIDTH/4.0
            y1 -= LINE_WIDTH/4.0
            node.rectangle = x0, x1, y0, y1
            context.arc(x0 + RADIUS, y0 + RADIUS, RADIUS,
                        - math.pi, - math.pi / 2.0)
            context.rel_line_to(x1 - x0 - 2*RADIUS, 0)
            context.arc(x1 - RADIUS, y0 + RADIUS, RADIUS,
                        - math.pi / 2.0, 0)
            context.rel_line_to(0, y1 - y0 - 2*RADIUS)
            context.arc(x1 - RADIUS, y1 - RADIUS, RADIUS,
                        0, math.pi / 2.0)
            context.rel_line_to(- x1 + x0 + 2*RADIUS, 0)
            context.arc(x0 + RADIUS, y1 - RADIUS, RADIUS,
                        math.pi / 2.0, math.pi)
            context.close_path()
            node.cairo_box_path = context.copy_path()
        else:
            context.append_path(node.cairo_box_path)
        context.stroke_preserve()

        gradient = cairo.LinearGradient(0, y0, 0, y1)

        gradient.add_color_stop_rgb(0.0, *colors[0])
        gradient.add_color_stop_rgb(1.0, *colors[1])
        context.set_source(gradient)
        context.fill()

        if is_selected:
            context.set_source_rgb(1, 1, 1)
        else:
            context.set_source_rgb(0, 0, 0)
        if first_time:
            name = sanitize_string(node.get_name())
            size = human_unit(node.size)
            position = x0, x1, y0, y1
            attempt = lambda text, pos, *flags: \
                self._make_draw_labels_lambda(context, text, pos, *flags) or \
                self._make_draw_labels_lambda(context, text,
                               (pos[0] - 1, pos[1] + 1, pos[2] - 1, pos[3] + 1),
                               *flags)
            node.draw_labels_lambda = attempt(name + '\n' + size, position) or \
                                attempt(name + ' ' + size, position, False) or \
                                attempt(name, position) or \
                                attempt(size, position) or \
                                (lambda context: None)
        node.draw_labels_lambda(context)

    @staticmethod
    def _intersect(clip, x0, x1, y0, y1):
        cx0, cx1, cy0, cy1 = clip.x, clip.x + clip.width, \
                             clip.y, clip.y + clip.height
        return x0 <= cx1 and x1 >= cx0 and y0 <= cy1 and y1 >= cy0

    def _draw_boxes(self, context, clip, node, depth, offset, interpol):
        w = self.allocation.width
        h = self.allocation.height
        x0 = depth * (w - 1.0) / (self.tree.height or 1)
        x1 = (depth + 1.0) * (w - 1.0) / (self.tree.height or 1)
        y0 = (h - 1.0) * offset / self.tree.root.size
        y1 = (h - 1.0) * (offset + node.size) / self.tree.root.size

        if self._intersect(clip, x0, x1, y0, y1):
            self._draw_box(context, x0, x1, y0, y1, node, interpol)
        depth += 1
        for child in node.children:
            self._draw_boxes(context, clip, child, depth, offset, interpol)
            offset += child.size

    def _draw(self, context, clip):
        max_text_height_before = self.max_text_height
        if self.tree.root.children:
            max_size = max(self.tree.root.children[0].size,
                           self.tree.root.children[-1].size)
            min_size = self.tree.root.minimum_node_size()
            diff = max(1, max_size - min_size)
            def interpol(small, big, x):
                return small + (x - min_size) * (big - small) / diff
        else:
            def interpol(small, big, x):
                return small
        context.set_line_width(LINE_WIDTH)
        offset = 0
        for child in self.tree.root.children or [self.tree.root]:
            if child.size:
                self._draw_boxes(context, clip, child, 0, offset, interpol)
                offset += child.size
        if self.max_text_height != max_text_height_before:
            self.schedule_new_tree()

    def _expose_event(self, event):
        context = self.window.cairo_create()

        # set a clip region for the expose event
        context.rectangle(event.area.x, event.area.y,
                          event.area.width, event.area.height)
        context.clip()

        self._draw(context, event.area)
        return False

    def max_number_of_nodes(self):
        return max(2, self.allocation.height / self.max_text_height)

    def _get_actual_min_size(self):
        min_size = self.min_size
        if min_size == 'auto':
            min_size = self.tree.root.size * self.min_size_requested()
        return int(min_size)

    def _zoom(self, func):
        min_size = self._get_actual_min_size()
        self.min_size = func(min_size)
        self.schedule_new_tree()

    def zoom_fit(self):
        self._zoom(lambda min_size: 'auto')

    def zoom_in(self):
        self._zoom(lambda min_size: min_size / 1.5)

    def zoom_out(self):
        self._zoom(lambda min_size: min_size * 1.5)
