# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

from pysize.ui.utils import UINotAvailableException
from pysize.core.signals import install_sigquit_handler, install_sigint_handler

def run(options, args):
    try:
        import cairo
        import pygtk
        pygtk.require('2.0')
        import gtk
        assert gtk.pygtk_version >= (2, 8)
    except Exception:
        raise UINotAvailableException
    install_sigquit_handler()
    install_sigint_handler()
    from pysize.ui.gtk.pysize_window import PysizeWindow
    PysizeWindow(options, args)
    gtk.main()
