# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import gtk
import pango

help_text = gtk.TextBuffer()
normal = help_text.create_tag(scale=pango.SCALE_LARGE)
big = help_text.create_tag(scale=pango.SCALE_X_LARGE, weight=pango.WEIGHT_BOLD)
blue = help_text.create_tag(scale=pango.SCALE_LARGE, background='#7676DB')
red = help_text.create_tag(scale=pango.SCALE_LARGE, background='#FA7F7F')
cyan = help_text.create_tag(scale=pango.SCALE_LARGE, background='#85F8F8')

def add(text, tag=normal, bullet=False):
    if bullet:
        add(u'\u2022 ')
    help_text.insert_with_tags(help_text.get_end_iter(), text, tag)

def show_help():
    dialog = gtk.Dialog(title='Pysize Help',
                        buttons=(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE))
    dialog.connect('response', lambda widget, event: widget.destroy())
    view = gtk.TextView(buffer=help_text)
    view.set_cursor_visible(False)
    view.set_editable(False)
    view.set_wrap_mode(gtk.WRAP_WORD)
    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    scroll.add(view)
    dialog.set_default_size(400, 500)
    dialog.vbox.add(scroll)
    dialog.show_all()

################################################################################

add('For each parent directory box, its files and sub-directories are ' +
'vertically sorted by size on its right.\n')

add('\nColors\n\n', tag=big)

add('Darker colors indicate bigger files or directories.\n', bullet=True)
add('Shades of blue represent directories.\n', tag=blue, bullet=True)
add('Files are shown in red.\n', tag=red, bullet=True)
add('Cyan boxes aggregate files and directories too small for a single box.\n',
    tag=cyan, bullet=True)

add('\nNavigation\n\n', tag=big)

add('The directory to display can be selected in the Open dialog, ' +
'accessible from the menu or the toolbar button.\n')
add('\nOnce a directory is displayed, it is possible to browse it by double ' +
'clicking on the boxes. ')
add('By default, boxes are sized so that the whole analyzed directory fits ' +
'the window. Altering the zoom level will make scrollbars appear and will ' +
'let more files and directories be displayed.\n')
add('\nAn history is available both with the Back/Forward buttons and the ' +
'History menu, it can be used to get back to previously displayed folders.\n')

add('\nDeletion\n\n', tag=big)

add('Files and directories can be selected by clicking on them. ')
add('Selections can be combined with the Control or Shift key. ')
add('Selected files and directories can be scheduled for deletion using the ' +
'contextual menu.\n')
add('Then, the "Deleted Files" window accessible from the View menu can be ' +
'used to actually delete or restore those files and directories.\n')
add('\nWithout this confirmation step, no file or directory will be deleted ' +
'by pysize\n')

add('\nCommand line\n\n', tag=big)

add('A curses and ASCII modes are respectively available with the ' +
'--ui=curses and --ui=ascii command line options. See the --help option for ' +
'more detail.')

