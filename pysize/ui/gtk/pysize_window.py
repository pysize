# -*- coding: UTF-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import pygtk
pygtk.require('2.0')
import gtk
assert gtk.pygtk_version >= (2, 8)
import gobject
import time
import shutil

from pysize.ui.gtk.gazpacho_loader.loader import ObjectBuilder

from pysize.ui.gtk.pysize_widget import PysizeWidget
from pysize.ui.gtk.help import show_help
from pysize.ui.utils import human_unit, sanitize_string
from pysize.core import compute_size
from pysize.core.compute_size import size_observable
from pysize.core import history
from pysize.core.deletion import get_deleted, restore
from pysize.version import VERSION

def hover_changed(main_widget, hover_node, status_bar):
    status_bar.pop(0)
    nodes = main_widget.get_selected_nodes()
    if not nodes:
        if hover_node:
            nodes = [hover_node]
        else:
            nodes = [main_widget.tree.root]
    size = sum([node.size for node in nodes])
    text = ' + '.join([node.get_fullname() for node in nodes])
    label = human_unit(size) + ' | ' + sanitize_string(text, max_length=1000)
    status_bar.push(0, label)

def size_observer(progress_bar):
    now = time.time()
    if now - size_observer.last_pulse > 0.04:
        size_observer.last_pulse = now
        gobject.idle_add(lambda: progress_bar.pulse())
size_observer.last_pulse = time.time()

def hide_pbar(progress_bar, visible):
    if visible:
        progress_bar.show()
    else:
        progress_bar.hide()

def update_action(zoom_fit_action, main_widget):
    # Enable if the zoom is not already 'auto' and there is a tree
    enable = main_widget.min_size != 'auto' and \
             not not main_widget.tree.root.get_fullpaths()
    zoom_fit_action.set_sensitive(enable)

def update_title(window, main_widget):
    root = main_widget.tree.root
    window.set_title('Pysize - %s | %s' %
                     (human_unit(root.size), sanitize_string(root.get_name())))

def update_back_action(back_action, next_insertion_index):
    back_action.set_sensitive(next_insertion_index > 1)

def update_forward_action(forward_action, next_insertion_index, history):
    forward_action.set_sensitive(next_insertion_index < len(history))

def update_hist_menu(main_widget, hist_menu, next_insertion_index, hist_list):
    class activator(object):
        def __init__(self, index):
            self.index = index

        def activate(self, unused_menu_item):
            main_widget.set_paths(history.go_to_history(self.index))

    for nr, item in enumerate(hist_menu):
        if nr > 2: # Keep 'Back' and 'Forward'
            hist_menu.remove(item)
    item = gtk.SeparatorMenuItem()
    item.show()
    hist_menu.add(item)
    for nr, (paths, name) in enumerate(hist_list):
        # We want underscores, not mnemonics
        name = sanitize_string(name, max_length=100).replace('_', '__')
        item = gtk.CheckMenuItem(name)
        if nr + 1  == next_insertion_index:
            item.set_active(True)
        item.connect('activate', activator(nr).activate)
        item.set_draw_as_radio(True)
        item.show()
        hist_menu.add(item)

def show_deleted_files(dialog, treeview):
    model = treeview.get_model()
    model.clear()
    for path in get_deleted():
        size = human_unit(compute_size.slow(path, account_deletion=False))
        model.append((size, path))
    dialog.run()
    dialog.hide()

def iter_selection(treeview):
    model = treeview.get_model()
    selection = treeview.get_selection().get_selected_rows()[1]
    if selection:
        selection.reverse()
        for treepath in selection:
            treeiter = model.get_iter(treepath)
            path = model.get_value(treeiter, 1)
            yield treeiter, path

def deleted_files_update_selection(treeview, restore_button, delete_button,
                                   deleted_files_size):
    selected_paths = [s[1] for s in iter_selection(treeview)]
    sensitive = len(selected_paths) > 0
    restore_button.set_sensitive(sensitive)
    delete_button.set_sensitive(sensitive)
    selected_paths = selected_paths or get_deleted()
    total = compute_size.slow_sum(selected_paths, account_deletion=False)
    deleted_files_size.set_text(human_unit(total))

def restore_deleted_files(treeview, main_widget):
    if treeview.get_selection().count_selected_rows() > 0:
        model = treeview.get_model()
        for treeiter, path in iter_selection(treeview):
            restore(path)
            model.remove(treeiter)
        main_widget.schedule_new_tree()

def delete_deleted_files(treeview, main_widget):
    if treeview.get_selection().count_selected_rows() <= 0:
        return;
    dialog = gtk.MessageDialog(parent=None, flags=gtk.DIALOG_MODAL,
                               type=gtk.MESSAGE_WARNING,
                               buttons=gtk.BUTTONS_YES_NO,
                               message_format='The selected will be permanently'
                               ' deleted, do you really want to delete them?')
    response = dialog.run()
    dialog.destroy()
    if response == gtk.RESPONSE_YES:
        model = treeview.get_model()
        for treeiter, path in iter_selection(treeview):
            restore(path)
            try:
                if os.path.isdir(path):
                    def rmtree_error(function, path, excinfo):
                        print excinfo[1]
                    shutil.rmtree(path, onerror=rmtree_error)
                else:
                    os.remove(path)
            except OSError, e:
                print e
            model.remove(treeiter)
        main_widget.schedule_new_tree()

def show_about():
    about = gtk.AboutDialog()
    about.set_name('Pysize')
    about.set_version(VERSION)
    about.set_copyright('Copyright © 2006-2007 Guillaume Chazarain')
    about.set_website('http://guichaz.free.fr/pysize')
    logo_filename = os.path.join(os.path.dirname(__file__), 'logo.svg')
    try:
        logo = gtk.gdk.pixbuf_new_from_file(logo_filename)
        logo = logo.scale_simple(200, 200, gtk.gdk.INTERP_HYPER)
        about.set_logo(logo)
    except Exception, e:
        # Not showing the logo is harmless
        print e
    about.run()
    about.destroy()

def quit_gui():
    if get_deleted():
        dialog = gtk.MessageDialog(parent=None, flags=gtk.DIALOG_MODAL,
                                   type=gtk.MESSAGE_WARNING,
                                   buttons=gtk.BUTTONS_YES_NO,
                                   message_format='Some files have been '
                                   'scheduled for deletion, but have not been '
                                   'deleted. Do you still want to quit?')
        response = dialog.run()
        dialog.destroy()
        if response != gtk.RESPONSE_YES:
            return True

    gtk.main_quit()
    return True

class PysizeWindow(object):
    def __init__(self, options, args):
        create_widget = lambda **unused_kwargs: PysizeWidget(options, args)
        glade_filename = os.path.join(os.path.dirname(__file__),
                                      'main_window.glade')
        builder = ObjectBuilder(glade_filename,
                                custom = {'pysize_widget': create_widget})

        main_widget = builder.get_widget('main_widget')
        status_bar = builder.get_widget('status_bar')
        main_widget.connect('hover_changed', hover_changed, status_bar)

        progress_bar = builder.get_widget('progress_bar')
        size_observable.add_observer(lambda: size_observer(progress_bar))
        pbar_hider = lambda w, building: hide_pbar(progress_bar, building)
        main_widget.connect('building-tree-state-changed', pbar_hider)

        max_depth = builder.get_widget('max_depth')
        spin_init = lambda w: max_depth.set_value(main_widget.options.max_depth)
        main_widget.connect('realize', spin_init)

        ui_manager = builder.get_widget('uimanager')
        zoom_fit_action = ui_manager.get_action('ui/toolbar/ZoomFit')
        action_updater = lambda w, building: update_action(zoom_fit_action, w)
        main_widget.connect('building-tree-state-changed', action_updater)

        # Disable actions when the tree is empty: pysize launched with no arg
        class disabler(object):
            def __init__(self, action):
                self.action = action
            def disable(self, main_widget, building):
                sensitive = not not main_widget.tree.root.get_fullpaths()
                self.action.set_sensitive(sensitive)
        for path in 'ui/toolbar/ParentDirectory', \
                    'ui/menubar/ActionsMenu/QuickRefresh', \
                    'ui/menubar/ActionsMenu/CompleteReload', \
                    'ui/toolbar/ZoomIn', 'ui/toolbar/ZoomOut':
            action = ui_manager.get_action(path)
            action_disabler = disabler(action).disable
            main_widget.connect('building-tree-state-changed', action_disabler)

        back_action = ui_manager.get_action('ui/toolbar/Back')
        back_action.set_sensitive(False)
        update_back = lambda n, h: update_back_action(back_action, n)
        history.history_observable.add_observer(update_back)

        forward_action = ui_manager.get_action('ui/toolbar/Forward')
        forward_action.set_sensitive(False)
        update_forw = lambda n, h: update_forward_action(forward_action, n, h)
        history.history_observable.add_observer(update_forw)

        hist_menu_action = ui_manager.get_action('ui/menubar/HistoryMenu')
        hist_menu = hist_menu_action.get_proxies()[0].get_submenu()
        upd_hist = lambda n, h: update_hist_menu(main_widget, hist_menu, n, h)
        history.history_observable.add_observer(upd_hist)

        toolbar = builder.get_widget('toolbar')
        toolbar_remaining = builder.get_widget('toolbar_remaining')
        toolbar_remaining.unparent()
        tool_item = gtk.ToolItem()
        tool_item.add(toolbar_remaining)
        toolbar.insert(tool_item, -1)

        self.add_refresh_buttons(toolbar, ui_manager)

        deleted_files_dialog = builder.get_widget('deleted_files_dialog')
        deleted_files_treeview = builder.get_widget('deleted_files_treeview')
        deleted_files_restore = builder.get_widget('deleted_files_restore')
        deleted_files_delete = builder.get_widget('deleted_files_delete')
        deleted_files_size = builder.get_widget('deleted_files_size')
        deleted_files_cb = lambda: deleted_files_update_selection(
            deleted_files_treeview, deleted_files_restore, deleted_files_delete,
            deleted_files_size)
        deleted_files_event = lambda *args: gobject.idle_add(deleted_files_cb)
        deleted_files_treeview.get_selection().set_mode(gtk.SELECTION_MULTIPLE)

        logo_filename = os.path.join(os.path.dirname(__file__), 'logo.svg')
        try:
            gtk.window_set_default_icon_from_file(logo_filename)
        except Exception, e:
            # Not showing the logo is harmless
            print e

        callbacks = {
            'quit_action': lambda *args: quit_gui(),
            'quick_refresh_action': lambda w: main_widget.schedule_new_tree(),
            'complete_reload_action': lambda w: main_widget.complete_reload(),
            'zoom_fit_action': lambda w: main_widget.zoom_fit(),
            'zoom_in_action': lambda w: main_widget.zoom_in(),
            'zoom_out_action': lambda w: main_widget.zoom_out(),
            'parent_directory_action': lambda w: main_widget.parent_directory(),
            'open_action': lambda w: main_widget.open(),
            'back_action': lambda w: main_widget.set_paths(history.back()),
            'forw_action': lambda w: main_widget.set_paths(history.forward()),
            'deleted_files_action': lambda w:
               show_deleted_files(deleted_files_dialog, deleted_files_treeview),
            'close_deleted_files': lambda b: deleted_files_dialog.hide(),
            'restore_deleted_files': lambda b:
               restore_deleted_files(deleted_files_treeview, main_widget),
            'delete_deleted_files': lambda b:
               delete_deleted_files(deleted_files_treeview, main_widget),
            'max_depth_changed': main_widget.max_depth_changed,
            'about_action': lambda a: show_about(),
            'help_action': lambda a: show_help(),
            'deleted_files_change_selection': deleted_files_event
        }
        builder.signal_autoconnect(callbacks)

        window = builder.get_widget('main_window')
        title_updater = lambda w, tree: update_title(window, main_widget)
        main_widget.connect('building-tree-state-changed', title_updater)
        window.show_all()

    def add_refresh_buttons(self, toolbar, ui_manager):
        quick = gtk.MenuToolButton(stock_id='')
        tooltips = gtk.Tooltips()
        quick_act = ui_manager.get_action('ui/menubar/ActionsMenu/QuickRefresh')
        quick_act.connect_proxy(quick)
        quick.set_tooltip(tooltips, quick_act.get_property('tooltip'))
        menu = gtk.Menu()
        quick.set_menu(menu)
        complete = gtk.MenuItem()
        cmp_act = ui_manager.get_action('ui/menubar/ActionsMenu/CompleteReload')
        cmp_act.connect_proxy(complete)
        tooltips.set_tip(complete, cmp_act.get_property('tooltip'))
        menu.add(complete)
        toolbar.insert(quick, 1)
        # The gtk.Tooltips object need to be kept referenced, otherwise it will
        # be garbage collected and will destroy its associated tooltips.
        PysizeWindow.__tooltips__ = tooltips

