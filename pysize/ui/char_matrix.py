# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import math
from pysize.ui.utils import human_unit, sanitize_string

# A character is composed of 9 positions
# 012
# 345
# 678
#
# 9: selection bit
#
# Example :
# '+' = 1, 3, 4, 5, 7 = (1<<1) + (1<<3) + (1<<4) + (1<<5) + (1<<7) = 186

HLINE_START = (1<<4) + (1<<5)
HLINE_END = (1<<3) + (1<<4)
HLINE = HLINE_START | HLINE_END

VLINE_START = (1<<4) + (1<<7)
VLINE_END = (1<<1) + (1<<4)
VLINE = VLINE_START | VLINE_END

MASK = (1<<9) - 1
SELECTED = 1<<9

class CharMatrix(object):
    """A 2D array of characters."""
    def __init__(self, width, height, tree, selected_node=None):
        self.width = width
        self.height = height
        self.tree = tree
        self.selected_node = selected_node
        self.matrix = [[0 for i in xrange(width)] for j in xrange(height)]
        self._horizontal_line(0, 0, width)
        self._horizontal_line(0, height - 1, width)
        self._vertical_line(0, 0, height)
        self._vertical_line(width - 1, 0, height)

        if not tree.root:
            return

        offset = 0
        total_size = tree.root.size
        for child in tree.root.children or [tree.root]:
            if child.size:
                offset += self._draw_boxes(child, 0, offset,
                                           self.height - 1 - offset, total_size)
                total_size -= child.size

    def _compose_point(self, x, y, new):
        self.matrix[y][x] |= new

    def _horizontal_line(self, x, y, length, mask=0):
        self._compose_point(x, y, HLINE_START | mask)
        for i in xrange(x + 1, x + length - 1):
            self._compose_point(i, y, HLINE | mask)
        self._compose_point(x + length - 1, y, HLINE_END | mask)

    def _vertical_line(self, x, y, length, mask=0):
        self._compose_point(x, y, VLINE_START | mask)
        for i in xrange(y + 1, y + length - 1):
            self._compose_point(x, i, VLINE | mask)
        self._compose_point(x, y + length - 1, VLINE_END | mask)

    def _draw_string(self, x, y, length, string):
        print_string = sanitize_string(string, max_length=length)
        self.matrix[y][x:x+len(print_string)] = print_string

    def _draw_box(self, x0, x1, y0, y1, node, mask):
        node.rectangle = x0, x1, y0, y1
        length = x1 - x0
        name = node.get_name()
        name_length = len(name)
        size = human_unit(node.size)
        if y1 > y0:
            self._horizontal_line(x0, y0, length + 1, mask)
            self._horizontal_line(x0, y1, length + 1, mask)
            self._vertical_line(x0, y0, y1 - y0 + 1, mask)
            self._vertical_line(x1, y0, y1 - y0 + 1, mask)

        if y1 > y0 + 1:
            self._draw_string(x0 + 1, (y0 + y1) / 2, length - 1, name)

        if y1 > y0 + 2:
            self._draw_string(x0 + 1, (y0 + y1) / 2 + 1, length - 1, size)
        elif name_length + 1 + len(size) < length:
            self._draw_string(x0 + 2 + name_length, (y0 + y1) / 2, length - 1,
                              size)

    def _draw_boxes(self, node, depth, offset, length, total_size):
        x0 = depth * (self.width - 1) / (self.tree.height or 1)
        x1 = (depth + 1) * (self.width - 1) / (self.tree.height or 1)
        y0 = offset
        y1 = offset + min(length, int(math.ceil(float(node.size) * length /
                                                total_size)))

        if node == self.selected_node:
            mask = SELECTED
        else:
            mask = 0

        self._draw_box(x0, x1, y0, y1, node, mask)
        depth += 1
        child_offset = 0
        total_size = node.size
        for child in node.children:
            child_offset += self._draw_boxes(child, depth,
                                             offset + child_offset,
                                             y1 - y0 - child_offset,
                                             total_size)
            total_size -= child.size
        return y1 - y0
