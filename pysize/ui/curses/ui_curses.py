# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import curses
import os
import sys
import time

from pysize.ui.utils import human_unit, update_progress
from pysize.ui.utils import sanitize_string, UINotAvailableException
from pysize.ui.char_matrix import CharMatrix, MASK, SELECTED
from pysize.core.pysize_fs_tree import pysize_tree
from pysize.core.pysize_fs_node import pretty_paths
from pysize.core.compute_size import size_observable
from pysize.core.signals import install_sigquit_handler

class _CursesApp(object):
    """The curses UI."""
    def __init__(self, win, options, args):
        self.window = win
        self.options = options
        self.max_depth = options.max_depth
        self.tree = None
        size_observable.add_observer(self.draw_star)
        self._set_paths(args)
        self._init_curses()
        self.last_star = 0
        self.last_star_time = time.time()

    def draw_star(self):
        progress = update_progress()
        if progress:
            self.status_win.insch(0, self.width - 1, progress, curses.A_REVERSE)
            self.status_win.refresh()

    def _init_curses(self):
        curses.use_default_colors()
        curses.start_color()
        try:
            curses.curs_set(0)
        except curses.error:
            # This call can fail with misconfigured terminals, for example
            # TERM=xterm-color. This is harmless
            pass

        def dots(*positions):
            return sum(map(lambda n: 1<<n, positions))

        self.MATRIX_TO_CURSES = {
            dots(): ' ',
            dots(3, 4, 5): curses.ACS_HLINE,
            dots(1, 4, 7): curses.ACS_VLINE,
            dots(1, 3, 4, 5, 7): curses.ACS_PLUS,
            dots(4, 5, 7): curses.ACS_ULCORNER,
            dots(3, 4, 7): curses.ACS_URCORNER,
            dots(1, 4, 5): curses.ACS_LLCORNER,
            dots(1, 3, 4): curses.ACS_LRCORNER,
            dots(3, 4, 5, 7): curses.ACS_TTEE,
            dots(1, 3, 4, 5): curses.ACS_BTEE,
            dots(1, 4, 5, 7): curses.ACS_LTEE,
            dots(1, 3, 4, 7): curses.ACS_RTEE
        }

        try:
            self.status_win = curses.newwin(0, 0)
            self.panel = curses.newwin(0, 0)
            self._resize()
        except curses.error:
            print 'Cannot create windows'
            sys.exit(1)

    def _resize(self):
        """Called when the terminal size changes."""
        self.height, self.width = self.window.getmaxyx()
        self.height -= 1 # Status bar
        self.need_tree = True
        self.status_win.resize(1, self.width)
        self.status_win.mvwin(self.height, 0)
        self.panel.resize(self.height, self.width)
        self._set_paths(self.paths)
        self._refresh()

    def _redraw(self):
        self.window.refresh()
        self.status_win.erase()
        self.status_win.hline(0, 0, ord(' ') | curses.A_REVERSE, self.width)
        if not self.tree:
            status_line = '[exploring ' + pretty_paths(self.paths) + ']'
        else:
            status_line = self.tree.root.get_name() + ' '
            status_line += human_unit(self.tree.root.size)
        END = ' - Pysize'
        if len(status_line) + len(END) > self.width:
            status_line = sanitize_string(status_line,
                                          max_length=self.width)
        else:
            status_line = sanitize_string(status_line + END)

        self.status_win.insstr(status_line.encode('UTF-8'), curses.A_REVERSE)
        self.status_win.refresh()
        self.panel.erase()
        if self.need_tree:
            # -1 accounts for the last horizontal line
            self.tree = pysize_tree(self.paths, self.max_depth,
                                    2.0 / (self.height - 1), self.options)
            self.need_tree = False
        if not self.tree.root:
            return

        self._draw_matrix(CharMatrix(self.width, self.height, self.tree,
                                     self.selected_node))

        self.panel.refresh()

    def _refresh(self):
        while True:
            try:
                self._redraw()
                break
            except curses.error, e:
                print e
                time.sleep(1)

    def _set_paths(self, paths):
        self.selected_node = None
        self.selection = None
        self.paths = paths
        self.need_tree = True

    def _next_step(self):
        self._set_paths([self.selected_node.get_dirname()])

    def _select(self, function):
        if not self.selected_node:
            if self.tree.root.children:
                self.selected_node = self.tree.root.children[0]
            else:
                # Forcing browsing to the left
                self._set_paths([self.tree.root.get_dirname()])
            return
        selected = function(self.selected_node)
        if not selected:
            if function == self.tree.get_first_child and \
                    self.selected_node.is_dir():
                # Browsing to the right
                self._next_step()
            return
        self.selected_node = selected
        if self.selected_node == self.tree.root:
            # Browsing to the left
            self._set_paths([self.tree.root.get_dirname()])

    def _selected(self):
        if self.selected_node:
            self._set_paths(self.selected_node.get_fullpaths())

    def run(self):
        """The main dispatcher."""
        key_bindings = {
            ord('q'): lambda:
                sys.exit(0),

            curses.KEY_RESIZE: lambda:
                self._resize(),

            curses.KEY_DOWN: lambda:
                self._select(self.tree.get_next_sibling),

            curses.KEY_UP: lambda:
                self._select(self.tree.get_previous_sibling),

            curses.KEY_LEFT: lambda:
                self._select(self.tree.get_parent),

            curses.KEY_RIGHT: lambda:
                self._select(self.tree.get_first_child),

            ord('\n'): lambda:
                self._selected()
        }

        while True:
            self._refresh()
            c = self.window.getch()
            action = key_bindings.get(c, lambda: None)
            action()

    def _draw_matrix(self, matrix):
        for y, line in enumerate(matrix.matrix):
            for x, char in enumerate(line):
                if isinstance(char, int):
                    selected = (char & SELECTED) != 0
                    char &= MASK
                    char = self.MATRIX_TO_CURSES[char]
                    if selected:
                        char |= curses.A_REVERSE
                    self.panel.insch(y, x, char)
                else:
                    try:
                        char = char.encode('UTF-8')
                    except UnicodeDecodeError:
                        pass
                    self.panel.insstr(y, x, char)

def _run_curses(win, options, args):
    install_sigquit_handler()
    try:
        args = args or [os.getcwd()]
        app = _CursesApp(win, options, args)
        app.run()
    except KeyboardInterrupt:
        sys.exit(0)

def run(options, args):
    if sys.stdin.isatty() and sys.stdout.isatty():
        curses.wrapper(_run_curses, options, args)
    else:
        raise UINotAvailableException
