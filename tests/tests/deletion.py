# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import unittest
from pysize.core import pysize_fs_node
from pysize.core import deletion
from fs_node import fake_options, create_root_node

class TestDeletion(unittest.TestCase):
    def testDeletion(self):
        node = create_root_node('/tmp/pysize_example_dir', 10, 10000,
                                fake_options())
        self.assertEqual(node.size, 4608000)
        parent = node.children[0].children[0].children[0]
        self.assertEqual(len(deletion.get_deleted()), 0)
        deletion.schedule([parent.children[1].children[0], parent.children[2]])
        node = create_root_node('/tmp/pysize_example_dir', 10, 10000,
                                fake_options())
        self.assertEqual(node.size, 4521984)
        self.assertEqual(len(deletion.get_deleted()), 11)
        deletion.restore(deletion.get_deleted()[0])
        node = create_root_node('/tmp/pysize_example_dir', 10, 10000,
                                fake_options())
        self.assertEqual(node.size, 4526080)
        deletion.schedule([parent])
        self.assertEqual(len(deletion.get_deleted()), 1)
        node = create_root_node('/tmp/pysize_example_dir', 10, 10000,
                                fake_options())
        self.assertEqual(node.size, 4128768)
        deletion.restore(parent.get_fullpaths()[0])
        self.assertEqual(len(deletion.get_deleted()), 0)
        node = create_root_node('/tmp/pysize_example_dir', 10, 10000,
                                fake_options())
        self.assertEqual(node.size, 4608000)

TESTS = (TestDeletion,)
