# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import unittest
from pysize.core import compute_size
from pysize.core.compute_size import size_observable
from pysize.core.pysize_global_fs_cache import drop_caches

class ErrorLogger(object):
    def __init__(self):
        self.log = ''

    def log_error(self, text, e):
        self.log += text
        self.log += ' '
        self.log += str(e)

def silent(text, e):
    pass

def fatal(text, e):
    print text
    raise e

class TestComputeSize(unittest.TestCase):
    def setUp(self):
        os.mkdir('/tmp/pysize_example_dir/unreadable_dir')
        os.chmod('/tmp/pysize_example_dir/unreadable_dir', 000)

    def tearDown(self):
        os.rmdir('/tmp/pysize_example_dir/unreadable_dir')

    def testSizeDir(self):
        drop_caches()
        logger = ErrorLogger()
        size = compute_size.slow('/tmp/pysize_example_dir',
                                 error_cb=logger.log_error)
        self.assertEqual(size, 4608000)
        self.assertEqual(logger.log, "size(/tmp/pysize_example_dir/" +
                                     "unreadable_dir) [Errno 13] Permission " +
                                     "denied: '/tmp/pysize_example_dir/" +
                                     "unreadable_dir'")

        # Retry using cached results
        size = compute_size.slow('/tmp/pysize_example_dir', error_cb=fatal)
        self.assertEqual(size, 4608000)

    def testSizeFile(self):
        drop_caches()
        size = compute_size.slow('/tmp/pysize_example_dir/unreadable_file',
                                 error_cb=fatal)
        self.assertEqual(size, 12288)

    def testSizeNoSuchPath(self):
        drop_caches()
        logger = ErrorLogger()
        size = compute_size.slow('/no such/ path', error_cb=logger.log_error)
        self.assertEqual(size, 0)
        self.assertEqual(logger.log, "size(/no such/ path) [Errno 2] No such " +
                                     "file or directory: '/no such/ path'")

    def testSizeCache(self):
        def inc_nr_calls():
            self.nr_calls += 1
        size_observable.add_observer(inc_nr_calls)
        drop_caches()
        self.nr_calls = 0
        self.assertEqual(compute_size.slow('/tmp/pysize_example_dir',
                                           error_cb=silent),
                         4608000)
        self.assertEqual(self.nr_calls, 10132)
        self.assertEqual(compute_size.slow('/tmp/pysize_example_dir',
                                           error_cb=fatal),
                         4608000)
        self.assertEqual(self.nr_calls, 10133)
        size_observable.del_observer(inc_nr_calls)
        self.assertEqual(compute_size.slow('/tmp/pysize_example_dir',
                                           error_cb=fatal),
                         4608000)
        self.assertEqual(self.nr_calls, 10133)

TESTS = (TestComputeSize,)
