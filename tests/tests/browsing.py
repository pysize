# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import unittest
from pysize.core.browsing import browsedir

LISTING = set(['hardlink1_4', 'bad_dirname$)\x0c\x02\xb5AUg\xdf\xda',
'hardlink1_2', 'hardlink1_3', 'hardlink1_1', 'dir_symlink', 'hardlink3_1',
'hardlink3_2', 'fifo', 'UTF-8_dir_\xc3\xa9_\xc3\xa0', 'empty_dir',
'dangling_symlink', 'unreadable_file', 'hardlink2_1', 'to_point', 'hardlink2_3',
'hardlink2_2', 'file_symlink'])

class TestBrowsing(unittest.TestCase):
    def assertListing(self, path):
        listing = set(map(os.path.basename, browsedir(path, True)))
        self.assertEqual(listing, LISTING)

    def testBrowsingOK(self):
        self.assertListing('/tmp/pysize_example_dir')

    def testBrowsingNoSuchPath(self):
        self.assertRaises(OSError, browsedir, '/no such/ path')

    def testBrowsingBadFile(self):
        self.assertRaises(OSError, browsedir,
                          '/tmp/pysize_example_dir/unreadable_file')

    def testBrowsingBadDir(self):
        self.assertRaises(OSError, browsedir,
                          '/tmp/pysize_example_dir/unreadable_dir')

    def testBrowsingBadSymlink(self):
        self.assertRaises(OSError, browsedir,
                          '/tmp/pysize_example_dir/dangling_symlink')

    def testBrowsingFileSymlink(self):
        self.assertRaises(OSError, browsedir,
                          '/tmp/pysize_example_dir/file_symlink')

    def testBrowsingDirSymlink(self):
        self.assertListing('/tmp/pysize_example_dir/to_point')

    def testBrowsingDirSymlinkSlash(self):
        self.assertListing('/tmp/pysize_example_dir/to_point/')

    def testCrossDevice(self):
        # Without cross device we should at least lose /proc
        self.assert_(len(browsedir('/', True)) > \
                     len(browsedir('/', False)))

TESTS = (TestBrowsing,)
