# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import unittest
from pysize.core import pysize_fs_node
from pysize.core.pysize_global_fs_cache import drop_caches

def create_root_node(fullpath, *args):
    return pysize_fs_node.create_node(None, [fullpath], [fullpath], *args)


class fake_options(object):
    def __init__(self):
        self.cross_device = True

def checksum(node):
    def value(n, op, name):
        res = op(n)
        if False:
            print type(n), n.basename, name, '=>', res
        return res
    checksum = 0
    for n in node:
        checksum += value(n, lambda n: n.compute_height(), 'height')
        checksum += value(n, lambda n: n.compute_depth(), 'depth')
        checksum += value(n, lambda n: n.minimum_node_size(), 'min_node_size')
        checksum += value(n, lambda n: len(n.get_dirname()), 'dirname')
        checksum += value(n, lambda n: n.is_dir() and 1 or 2, 'is_dir')
        checksum += value(n, lambda n: n.is_real() and 3 or 5, 'is_real')
        checksum += value(n, lambda n: len(n.get_fullname()), 'fullname')
        checksum += value(n, lambda n: len(','.join(n.get_fullpaths())), 'fp')
        checksum += value(n, lambda n: len(n.get_name()), 'get_name')
    return checksum

class TestFsNode(unittest.TestCase):
    def testTree(self):
        drop_caches()
        node = create_root_node('/tmp/pysize_example_dir', 10, 10000,
                                fake_options())
        self.assertEqual(node.get_fullname(), '/tmp/pysize_example_dir')
        self.assertEqual(node.compute_height(), 6)
        self.assertEqual(node.compute_depth(), 1)
        self.assertEqual(node.minimum_node_size(), 12288)
        self.assertEqual(node.get_dirname(), '/tmp')
        self.assert_(node.is_dir())
        self.assertEqual(checksum(node), 8866642)
        child = node.children[0]
        self.assertEqual(child.get_fullname(),
                         '/tmp/pysize_example_dir/UTF-8_dir_\xc3\xa9_\xc3\xa0')
        self.assertEqual(child.get_name(), 'UTF-8_dir_\xc3\xa9_\xc3\xa0/')

    def testForest(self):
        drop_caches()
        paths = ['/tmp/pysize_example_dir/dir_symlink/hardlinks/0/%d' % i for i
                                                                  in xrange(10)]
        node = pysize_fs_node.create_node(None, paths, paths, 10, 1000,
                                          fake_options())
        self.assertEqual(node.get_fullname(),
        '/tmp/pysize_example_dir/dir_symlink/hardlinks/0/{0,1,2,3,4,5,6,7,8,9}')
        self.assertEqual(node.compute_height(), 3)
        self.assertEqual(node.compute_depth(), 1)
        self.assertEqual(node.minimum_node_size(), 4096)
        self.assertEqual(node.get_dirname(),
                         '/tmp/pysize_example_dir/dir_symlink/hardlinks/0')
        self.failIf(node.is_dir())
        self.assertEqual(checksum(node), 502156)
        child = node.children[0]
        self.assertEqual(child.get_fullname(),
                         '/tmp/pysize_example_dir/dir_symlink/hardlinks/0/0')
        self.assertEqual(child.get_name(), '0/')

    def testFile(self):
        drop_caches()
        node = create_root_node('/tmp/pysize_example_dir/unreadable_file', 10,
                                10000, fake_options())
        self.assertEqual(node.get_name(),
                         '/tmp/pysize_example_dir/unreadable_file')
        self.assertEqual(checksum(node), 12434)

    def testInit(self):
        drop_caches()
        node = pysize_fs_node.create_node(None, None, None, None, None,
                                          fake_options())
        self.assertEqual(node.get_fullname(), '')

TESTS = (TestFsNode,)
