# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import unittest
from pysize.core import pysize_fs_tree
from fs_node import fake_options

class TestFsTree(unittest.TestCase):
    def testTree(self):
        tree = pysize_fs_tree.pysize_tree(['/tmp/pysize_example_dir'], 10, 0.02,
                                          fake_options())
        root = tree.root
        child = tree.get_first_child(tree.get_first_child(root))
        self.assertEqual(tree.get_previous_sibling(child), None)
        child = tree.get_first_child(tree.get_first_child(child))
        self.assertEqual(tree.get_previous_sibling(child), None)
        next = tree.get_next_sibling(child)
        self.assertEqual(tree.get_previous_sibling(next), child)
        count = 0
        while next:
            next = tree.get_next_sibling(next)
            count += 1
        self.assertEqual(count, 9)
        parent = tree.get_parent(tree.get_parent(child))
        next = tree.get_next_sibling(parent)
        self.assertEqual(tree.get_previous_sibling(next), parent)
        count = 0
        while next:
            next = tree.get_next_sibling(next)
            count += 1
        self.assertEqual(count, 1)
        parent = tree.get_parent(tree.get_parent(parent))
        self.assertEqual(parent, root)

TESTS = (TestFsTree,)
