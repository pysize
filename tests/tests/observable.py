# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import unittest
from pysize.core.observable import observable

class TestObservable(unittest.TestCase):
    def testObservableAddRemove(self):
        cb = lambda : 1/0
        o = observable()
        o.fire_observers()
        o.add_observer(cb)
        o.del_observer(cb)
        o.fire_observers()
        self.assertRaises(ValueError, o.del_observer, cb)

    def testObservableCopy(self):
        def cb(observer):
            observer.add_observer(cb)
            self.nr_calls += 1
        o = observable()
        o.add_observer(cb)
        o.add_observer(cb)
        self.nr_calls = 0
        o.fire_observers(o)
        self.assertEqual(self.nr_calls, 2)
        o.fire_observers(o)
        self.assertEqual(self.nr_calls, 6)
        o.fire_observers(o)
        self.assertEqual(self.nr_calls, 14)
        o.fire_observers(o)
        self.assertEqual(self.nr_calls, 30)


TESTS = (TestObservable,)
