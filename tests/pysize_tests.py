#!/usr/bin/env python
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# See the COPYING file for license information.
#
# Copyright (c) 2006, 2007, 2008 Guillaume Chazarain <guichaz@gmail.com>

import os
import tarfile
import unittest
import shutil
import sys
import optparse

import coverage

PYSIZE_EXAMPLE_PATH = '/tmp/pysize_example_dir'
ALL_TESTS = unittest.TestSuite()

def setup():
    if not os.path.exists('/tmp/pysize_example_dir'):
        tar = tarfile.open('pysize_example_dir.tar.bz2', 'r:bz2')
        print 'Extracting pysize_example_dir.tar.bz2'
        for tarinfo in tar:
            tar.extract(tarinfo, '/tmp')
        tar.close()

def cleanup():
    print 'Removing', PYSIZE_EXAMPLE_PATH
    shutil.rmtree(PYSIZE_EXAMPLE_PATH)

def import_tests():
    py_files = [p for p in os.listdir('tests') if p.endswith('.py')]
    tests = list(set([p[:p.index('.')] for p in py_files]))
    for name in tests:
        module = getattr(__import__('tests.' + name), name)
        for test in module.TESTS:
            suite = unittest.defaultTestLoader.loadTestsFromTestCase(test)
            ALL_TESTS.addTest(suite)

def parse_cmdline():
    parser = optparse.OptionParser()
    parser.add_option('--keep', '-k', action='store_true', dest='keep',
                      default=False, help='keep /tmp/pysize_example_dir')
    parser.add_option('--coverage', '-c', action='store_true', dest='coverage',
                      default=False, help='include coverage tests')
    options, args = parser.parse_args()
    if args:
        parser.error()
    return options

def end_coverage():
    coverage.stop()
    modules = []
    for name, module in sys.modules.iteritems():
        if name.startswith('pysize.') and module:
            path = '../' + name.replace('.', '/') + '.py'
            if os.path.exists(path):
                modules.append(module)
    coverage.report(modules)

def main():
    options = parse_cmdline()
    sys.path.insert(0, '..')
    if options.coverage:
        coverage.start()
    import_tests()
    try:
        setup()
        unittest.main(argv=[sys.argv[0], '-v'], defaultTest='ALL_TESTS')
    finally:
        if options.coverage:
            end_coverage()
        if not options.keep:
            cleanup()

if __name__ == '__main__':
    main()
